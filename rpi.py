from datetime import datetime
import socket
import json
import base64
from scramp import ScramClient
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives.asymmetric import dh
from cryptography.hazmat.primitives.serialization import PublicFormat, Encoding, load_pem_public_key

HOST = "192.168.1.20"  # The RPI's hostname or IP address
PORT = 5432  # The port used by the RPI
USER = "RPI"
PASSWORD = "SUPERSECRETPASS"
MECHANISM = ['SCRAM-SHA-256']
c = ScramClient(MECHANISM, USER, PASSWORD)

if __name__ == "__main__":
    new_file = datetime.now().strftime("patch-%Y%m%d-%H%M%s")

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
        conn, addr = s.accept()

        #SPAWN THE PROCESS 

        with conn:
            print(f'CONNECTED TO {addr}')
            ### KEY EXCHANGE
            ## AGREE ON PARAMETERS TO USE FOR DHM
            pn = dh.generate_parameters(5, 512)
            pn_numbers = pn.parameter_numbers()
            conn.sendall(json.dumps({"g": pn_numbers.g, "p": pn_numbers.p}).encode('utf-8'))

            private_key = pn.generate_private_key()
            public_key = private_key.public_key()
            key_to_send = public_key.public_bytes(encoding=Encoding.PEM, format=PublicFormat.SubjectPublicKeyInfo)
            conn.sendall(key_to_send)

            peer_public_key = load_pem_public_key(conn.recv(1024))
            shared_key = private_key.exchange(peer_public_key)

            cipher = Fernet(base64.b64encode(shared_key[:32]))

            ### MUTAL AUTHENTICATION
            try:
                client_first_message = cipher.encrypt(c.get_client_first().encode('utf-8'))
                print(f'THE CLIENT FIRST MESSAGE SENT IS {client_first_message}')
                conn.sendall(client_first_message)

                server_first_message = cipher.decrypt(conn.recv(1024)).decode('utf-8')
                print(f'THE SERVER FIRST MESSAGE RECV IS {server_first_message}')
                c.set_server_first(server_first_message)

                client_final_message = cipher.encrypt(c.get_client_final().encode('utf-8'))
                print(f'THE CLIENT FINAL MESSAGE SENT IS {client_final_message}')
                conn.sendall(client_final_message)

                server_final_message = cipher.decrypt(conn.recv(1024)).decode('utf-8')
                print(f'THE SERVER FINAL MESSAGE RECV IS {server_final_message}')
                c.set_server_final(server_final_message)
            except Exception:
                print('An error occured during authentication, closing connection!')
                conn.close()
            
            ## FILE TRANSFER
            file_byte_size = json.loads(cipher.decrypt(conn.recv(1024)).decode('utf-8'))['file_size']
            print(file_byte_size)

            conn.sendall('OK'.encode('utf-8'))

            bytes_recv = 0
            file_data = b''
            while bytes_recv < file_byte_size:
                data = cipher.decrypt(conn.recv(1024).decode('utf-8'))
                file_data += data
                bytes_recv += 1024

            with open(new_file+".py", 'wb') as recv_file:
                recv_file.write(data)