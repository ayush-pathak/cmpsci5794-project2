import os
import socket
import json
import base64
from scramp import ScramMechanism
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives.asymmetric import dh
from cryptography.hazmat.primitives.serialization import PublicFormat, Encoding, load_pem_public_key

HOST = "192.168.1.20"  # RPI address
PORT = 5432  # Port to listen on (non-privileged ports are > 1023)
USER = "RPI"
PASSWORD = "SUPERSECRETPASS"
### MIMIC DB TO STORE AUTH RELATED DETAILS
db = {}
m = ScramMechanism()
salt, stored_key, server_key, iteration_count = m.make_auth_info(PASSWORD)
db[USER] = salt, stored_key, server_key, iteration_count

### FUNCTION TO GET PASSWORD ADJACENT DETAILS
def auth_fn(username):
    return db[username]

#in main program

if __name__ == "__main__":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        print(f"Connected to RPI")
        ### KEY EXCHANGE
        KEY_PARAMS = json.loads(s.recv(1024).decode('utf-8'))
        pn_numbers = dh.DHParameterNumbers(KEY_PARAMS["p"], KEY_PARAMS["g"])
        pn = pn_numbers.parameters()

        private_key = pn.generate_private_key()
        public_key = private_key.public_key()
        key_to_send = public_key.public_bytes(encoding=Encoding.PEM, format=PublicFormat.SubjectPublicKeyInfo)
        s.sendall(key_to_send)
        
        peer_public_key = load_pem_public_key(s.recv(1024))
        shared_key = private_key.exchange(peer_public_key)

        cipher = Fernet(base64.b64encode(shared_key[:32]))

        ### MUTUAL AUTHENTICATION
        scramp_auth_server = m.make_server(auth_fn)
        try:
            client_first_message = cipher.decrypt(s.recv(1024)).decode('utf-8')
            print(f'THE CLIENT FIRST MESSAGE RECV IS {client_first_message}')
            scramp_auth_server.set_client_first(client_first_message)
            
            server_first_message = cipher.encrypt(scramp_auth_server.get_server_first().encode('utf-8'))
            print(f'THE SERVER FIRST MESSAGE SENT IS {server_first_message}')
            s.sendall(server_first_message)

            client_final_message = cipher.decrypt(s.recv(1024)).decode('utf-8')
            print(f'THE CLIENT FINAL MESSAGE RECV IS {client_final_message}')
            scramp_auth_server.set_client_final(client_final_message)

            server_final_message = cipher.encrypt(scramp_auth_server.get_server_final().encode('utf-8'))
            print(f'THE SERVER FINAL MESSAGE SENT IS {server_final_message}')
            s.sendall(server_final_message)
        except Exception:
            print('An error occured during authentication, RPI closed connection')
            s.close()
        
        ## FILE TRANSFER

        ## SEND FILE SIZE TO EXPECT
        file_byte_size = os.path.getsize("SOMEFILE.py")
        s.sendall(cipher.encrypt(json.dumps({'file_size': file_byte_size}).encode('utf-8')))

        file_data = open("SOMEFILE.py", mode='rb')

        if (s.recv(1024) != b'OK'):
            exit(-1)

        bytes_sent = 0
        while bytes_sent < file_byte_size:
            data = file_data.read(1024)
            s.sendall(cipher.encrypt(data))
            bytes_sent += 1024
        
        file_data.close()



