import os
import socket
import json
import base64
from scramp import ScramMechanism
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives.asymmetric import dh
from cryptography.hazmat.primitives.serialization import PublicFormat, Encoding, load_pem_public_key

UDP_HOST = "192.168.1.26"  #BASE address
UDP_PORT = 5432  # Port to listen on (non-privileged ports are > 1023)
RPI_UDP_HOST = '192.168.1.20'
RPI_UDP_PORT = 5432
USER = "RPI"
PASSWORD = "SUPERSECRETPASS"
### MIMIC DB TO STORE AUTH RELATED DETAILS
db = {}
m = ScramMechanism()
salt, stored_key, server_key, iteration_count = m.make_auth_info(PASSWORD)
db[USER] = salt, stored_key, server_key, iteration_count

### FUNCTION TO GET PASSWORD ADJACENT DETAILS
def auth_fn(username):
    return db[username]

#in main program

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind((UDP_HOST, UDP_PORT))


if __name__ == "__main__":
    # while(True):
        ### KEY EXCHANGE
    print("GETTING KEYS")
    KEY_PARAMS = json.loads(s.recvfrom(1024)[0].decode('utf-8'))
    print(KEY_PARAMS)
    pn_numbers = dh.DHParameterNumbers(KEY_PARAMS["p"], KEY_PARAMS["g"])
    pn = pn_numbers.parameters()

    private_key = pn.generate_private_key()
    public_key = private_key.public_key()
    key_to_send = public_key.public_bytes(encoding=Encoding.PEM, format=PublicFormat.SubjectPublicKeyInfo)
    s.sendto(key_to_send, (RPI_UDP_HOST, RPI_UDP_PORT))
    peer_public_key = load_pem_public_key(s.recvfrom(1024)[0])
    shared_key = private_key.exchange(peer_public_key)

    cipher = Fernet(base64.b64encode(shared_key[:32]))

    ### MUTUAL AUTHENTICATION
    scramp_auth_server = m.make_server(auth_fn)
    try:
        client_first_message = cipher.decrypt(s.recvfrom(1024)[0]).decode('utf-8')
        print(f'THE CLIENT FIRST MESSAGE RECV IS {client_first_message}')
        scramp_auth_server.set_client_first(client_first_message)
        
        server_first_message = cipher.encrypt(scramp_auth_server.get_server_first().encode('utf-8'))
        print(f'THE SERVER FIRST MESSAGE SENT IS {server_first_message}')
        s.sendto(server_first_message, (RPI_UDP_HOST, RPI_UDP_PORT))

        client_final_message = cipher.decrypt(s.recvfrom(1024)[0]).decode('utf-8')
        print(f'THE CLIENT FINAL MESSAGE RECV IS {client_final_message}')
        scramp_auth_server.set_client_final(client_final_message)

        server_final_message = cipher.encrypt(scramp_auth_server.get_server_final().encode('utf-8'))
        print(f'THE SERVER FINAL MESSAGE SENT IS {server_final_message}')
        s.sendto(server_final_message, (RPI_UDP_HOST, RPI_UDP_PORT))
    except Exception:
        print('An error occured during authentication, RPI closed connection')
        s.close()
        exit(-1)
    
    ## FILE TRANSFER

    ## SEND FILE SIZE TO EXPECT
    file_byte_size = os.path.getsize("SOMEFILE.py")
    s.sendto(cipher.encrypt(json.dumps({'file_size': file_byte_size}).encode('utf-8')), (RPI_UDP_HOST, RPI_UDP_PORT))

    file_data = open("SOMEFILE.py", mode='rb')

    if (s.recvfrom(1024)[0] != b'OK'):
        exit(-1)

    bytes_sent = 0
    while bytes_sent < file_byte_size:
        data = file_data.read(1024)
        s.sendto(cipher.encrypt(data), (RPI_UDP_HOST, RPI_UDP_PORT))
        bytes_sent += 1024
    
    file_data.close()



