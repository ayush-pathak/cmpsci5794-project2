from datetime import datetime
import socket
import json
import base64
from scramp import ScramClient
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives.asymmetric import dh
from cryptography.hazmat.primitives.serialization import PublicFormat, Encoding, load_pem_public_key

BASE_UDP_HOST = "192.168.1.26"  #BASE address
BASE_UDP_PORT = 5432  # Port to listen on (non-privileged ports are > 1023)
UDP_HOST = '192.168.1.20'
UDP_PORT = 5432

USER = "RPI"
PASSWORD = "SUPERSECRETPASS"
MECHANISM = ['SCRAM-SHA-256']
c = ScramClient(MECHANISM, USER, PASSWORD)

if __name__ == "__main__":
    new_file = datetime.now().strftime("patch-%Y%m%d-%H%M%s")

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.bind((UDP_HOST, UDP_PORT))
        ### KEY EXCHANGE
        ## AGREE ON PARAMETERS TO USE FOR DHM
        pn = dh.generate_parameters(5, 512)
        pn_numbers = pn.parameter_numbers()
        print({"g": pn_numbers.g, "p": pn_numbers.p})
        s.sendto(json.dumps({"g": pn_numbers.g, "p": pn_numbers.p}).encode('utf-8'), (BASE_UDP_HOST, BASE_UDP_PORT))
        print("sent key params")
        print("waiting for peer pub key")

        peer_public_key = load_pem_public_key(s.recvfrom(1024)[0])
        private_key = pn.generate_private_key()
        public_key = private_key.public_key()
        key_to_send = public_key.public_bytes(encoding=Encoding.PEM, format=PublicFormat.SubjectPublicKeyInfo)
        s.sendto(key_to_send, (BASE_UDP_HOST, BASE_UDP_PORT))

        shared_key = private_key.exchange(peer_public_key)

        cipher = Fernet(base64.b64encode(shared_key[:32]))

        ### MUTAL AUTHENTICATION
        try:
            client_first_message = cipher.encrypt(c.get_client_first().encode('utf-8'))
            print(f'THE CLIENT FIRST MESSAGE SENT IS {client_first_message}')
            s.sendto(client_first_message, (BASE_UDP_HOST, BASE_UDP_PORT))

            server_first_message = cipher.decrypt(s.recvfrom(1024)[0]).decode('utf-8')
            print(f'THE SERVER FIRST MESSAGE RECV IS {server_first_message}')
            c.set_server_first(server_first_message)

            client_final_message = cipher.encrypt(c.get_client_final().encode('utf-8'))
            print(f'THE CLIENT FINAL MESSAGE SENT IS {client_final_message}')
            s.sendto(client_final_message, (BASE_UDP_HOST, BASE_UDP_PORT))

            server_final_message = cipher.decrypt(s.recvfrom(1024)[0]).decode('utf-8')
            print(f'THE SERVER FINAL MESSAGE RECV IS {server_final_message}')
            c.set_server_final(server_final_message)
        except Exception:
            print('An error occured during authentication, closing section!')
            s.close()
        
        ## FILE TRANSFER
        file_byte_size = json.loads(cipher.decrypt(s.recvfrom(1024)[0]).decode('utf-8'))['file_size']
        print(file_byte_size)

        s.sendto('OK'.encode('utf-8'), (BASE_UDP_HOST, BASE_UDP_PORT))

        bytes_recv = 0
        file_data = b''
        while bytes_recv < file_byte_size:
            data = cipher.decrypt(s.recvfrom(1024)[0].decode('utf-8'))
            file_data += data
            bytes_recv += 1024

        with open(new_file+".py", 'wb') as recv_file:
            recv_file.write(data)