# CMPSCI5794-project2

## Objectives

- The objective of this project is to provide a simple implementation of mutual authentication between a base station and a IoT device, such that over-the-air file transfers can be safely conducted without leaving a channel vulnerable to attackers. We implement this mutual authentication via Salted Challenge Response Authentication Mechanism or SCRAM. This method allows for the client and server (IoT Device and Base Station) to both agree that they are communicating with the approriate entities. SCRAM opperates on the fact that both entities are able to calculate a "proof" with different stored information. If this process fails or throws an error, the entites will stop communication and the socket is closed, which safeguards the IoT device from attackers. Once this process succeeds, a file can be transfered to the device and the socket is closed after the file is received. All communication is encrypted using AES in CBC mode with an exchange shared key, derived by Diffie-Hellmann key exchange. 

## Installation

In order to be able to run the files, must install the required libraries used. Ensure that python and pip are installed. 

Run `pip3 install -r requirements.txt` while at the root of the project and it should install the requirements needed for the project

The required libraries are `scramp` and `cryptography`. `scramp` is a python library that abstracts away the creation and tracking of the actual 'proof' variable that is need for comparison between the two devices. The `cryptography` is used to implement the key exchange, encryption, and decryption of messages on the two devices. 

## Running the files

If you look at the files, we have a few places where certain private IP addresses are set.
In base_station.py, change the HOST variable to the private IP address on the network of the device that is acting as the base station.  In the rpi.py file, do the same as we did for the base station with the device that is acting as the IoT device.

For the UDP version of the file, in the base_station_udp.py file, you must change the UDP_HOST to the to the private IP address on the network of the device that is acting as the base station. For the RPI_UDP_HOST variable, you must change that to the private IP address of the device that is acting as the IoT device. In the rpi_udp.py file, you must update the BASE_UDP_HOST variable to the IP address of the base station, same with the UDP_HOST variables.

After making the required changes for your environment, to run the either file use the command `python (filename)`. Note that only the base_station_udp.py and rpi_udp.py will work with each other. The TCP version of the code does not have any suffix. 